#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import pydot
import math
from IbNetPyGraph import IbNetPyGraph


class Colosse(IbNetPyGraph):
    def __init__(self, ibNetDict, graph):
        IbNetPyGraph.__init__(self, ibNetDict, graph)

        self.nbFC = 2  # Start to 1
        self.nbLCbyFC = 7 + 1  # Start to 0
        self.nbRACK = 10  # Start to 1
        self.nbQNEMbyRACK = 4  # Start to 1
        self.nbM2 = 2  # Start to 1

        self.groupedNodes = ['QNEM', 'M2', 'Xyratex']

        # + 1 : Xyratex et + 3 : marge (optionnal)
        N = self.nbFC + self.nbLCbyFC * self.nbFC + self.nbRACK + self.nbRACK \
            * self.nbQNEMbyRACK + self.nbM2 + 1 + 3

        self.HSV_tuples = self.generateHSV(N)

    def createClusters(self):
        self.createClusterM9FCLC()
        self.createClusterRACKQNEM()
        self.createClusterM2Xyratex()

    def createClusterM9FCLC(self):
        for i in range(self.nbFC):
            self.dictSubGraph.setdefault(
                "M9-%d_FC" % (i + 1),
                {
                    'subgraph': pydot.Cluster(
                        "M9%dFC" % (i + 1),
                        fillcolor=",".join(self.HSV_tuples[self.i]),
                        style='filled',
                        tooltip="M9-%d_FC" % (i + 1),
                        id="M9-%d_FC" % (i + 1)),
                    'node': {
                        'color': ",".join(self.HSV_tuples[self.i]),
                        'pos': "%f,%f" %
                        (1 * math.sin((i / float(self.nbFC)) * (2 * math.pi)),
                         1 * math.cos((i / float(self.nbFC)) * (2 * math.pi))),
                        'len': 1
                    }
                })
            self.i += 1
            for j in range(self.nbLCbyFC):
                self.dictSubGraph.setdefault(
                    "M9-%d_LC%d" % (i + 1, j),
                    {
                        'subgraph': pydot.Cluster(
                            "M9%dLC%d" % (i + 1, j),
                            fillcolor=",".join(self.HSV_tuples[self.i]),
                            style='filled',
                            tooltip="M9-%d_LC%d" % (i + 1, j),
                            id="M9-%d_LC%d" % (i + 1, j)),
                        'node': {
                            'color': ",".join(self.HSV_tuples[self.i]),
                            'pos': "%f,%f" %
                            (5 * math.sin((((i * self.nbLCbyFC) + j) / float(self.nbLCbyFC * self.nbFC)) * (2 * math.pi)),
                             5 * math.cos((((i * self.nbLCbyFC) + j) / float(self.nbLCbyFC * self.nbFC)) * (2 * math.pi))),
                            'len': 18.4
                        }
                    })
                self.i += 1

    def createClusterRACKQNEM(self):
        for i in range(self.nbRACK):
            # To separate by RACK instead of by QNEM
            # dictSubGraph.setdefault(
            #     "RACK-1%02d" % (i + 1),
            #     {
            #         'subgraph': pydot.Cluster(
            #             "RACK1%02d" % (i + 1),
            #             fillcolor=",".join(HSV_tuples[self.i]),
            #             style='filled',
            #             size="100000,10"),
            #         'node': {
            #             'color': ",".join(HSV_tuples[self.i]),
            #             'pos': "%f,%f" %
            #             (10 * math.sin((i / float(nbRACK)) * (2 * math.pi)),
            #              10 * math.cos((i / float(nbRACK)) * (2 * math.pi))),
            #             'len': 10.0
            #         }
            #     })
            # self.i += 1

            for j in range(self.nbQNEMbyRACK):
                self.dictSubGraph.setdefault(
                    "RACK-1%02d_QNEM-%d" % (i + 1, j + 1),
                    {
                        'subgraph': pydot.Cluster(
                            "RACK1%02dQNEM%d" % (i + 1, j + 1),
                            fillcolor=",".join(self.HSV_tuples[self.i]),
                            style='filled',
                            tooltip="RACK-1%02d_QNEM-%d" % (i + 1, j + 1),
                            id="RACK-1%02d_QNEM-%d" % (i + 1, j + 1)),
                        'node': {
                            'color': ",".join(self.HSV_tuples[self.i]),
                            'pos': "%f,%f" %
                            (15 * math.sin((((i * self.nbQNEMbyRACK) + j) / float(self.nbRACK * self.nbQNEMbyRACK)) * (2 * math.pi)),
                             15 * math.cos((((i * self.nbQNEMbyRACK) + j) / float(self.nbRACK * self.nbQNEMbyRACK)) * (2 * math.pi))),
                            'len': 12.0
                        }
                    })
            # Generate a new color for each RACK
            self.i += (1 + self.nbQNEMbyRACK)

    def createClusterM2Xyratex(self):
        for i in range(self.nbM2):
            self.dictSubGraph.setdefault(
                "M2-%d" % (i + 1),
                {
                    'subgraph': pydot.Cluster(
                        "M2%d" % (i + 1),
                        fillcolor=",".join(self.HSV_tuples[self.i]),
                        style='filled',
                        tooltip="M2-%d" % (i + 1),
                        id="M2-%d" % (i + 1)),
                    'node': {
                        'color': ",".join(self.HSV_tuples[self.i]),
                        'pos': "%f,%f" %
                        (1 * math.sin((i / float(self.nbM2)) * (2 * math.pi)),
                         1 * math.cos((i / float(self.nbM2)) * (2 * math.pi))),
                        'len': 1
                    }
                })
            self.i += 1

        self.dictSubGraph.setdefault(
            "Xyratex_IS5035U1",
            {
                'subgraph': pydot.Cluster(
                    "XyratexIS5035U1",
                    fillcolor=",".join(self.HSV_tuples[self.i]),
                    style='filled',
                    tooltip="Xyratex_IS5035U1",
                    id="Xyratex_IS5035U1"),
                'node': {
                    'color': ",".join(self.HSV_tuples[self.i]),
                    'pos': "%f,%f" % (1, 0),
                    'len': 1
                }
            })
        self.i += 1

    def createInvNodes(self):
        for keygrp in self.dictSubGraph.keys():
            if "M9" in keygrp:
                #Noeud invisible pour rassembler les noeuds d'un meme cluster
                self.dictSubGraph[keygrp]['subgraph'].add_node(
                    pydot.Node(
                        keygrp,
                        style='invis',
                        #id='N' + keygrp,
                        #fillcolor=self.dictSubGraph[keygrp]['node']['color'],
                        #tooltip="Name: " + keygrp,
                        shape="point"
                    ))

                for node in self.dictSubGraph[keygrp]['subgraph'].get_node_list():
                    self.dictSubGraph[keygrp]['subgraph'].add_edge(
                        pydot.Edge(
                            node.get_name(),
                            keygrp,
                            style='invis',
                            weight=10,
                            len="0.01"))
            else:
                #Noeud invisible pour rassembler les enfants
                self.graph.add_node(pydot.Node(
                    keygrp + 'inv',
                    style="invis"))
