#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import pydot
import argparse
import time

from utilParser import readIbNet
import importlib


def main():
    parser = argparse.ArgumentParser(
        description='Generate svg file')

    parser.add_argument(
        "ibnetdiscover",
        help="ibNetDiscover file")
    parser.add_argument(
        "dst",
        help="Destination file to write graph")

    parser.add_argument(
        "-s", "--site",
        action="store",
        dest="adapter",
        default="IbNetPyGraph",
        type=str,
        help="Adapter to use for a site")

    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        dest="verbose",
        default=False,
        help="verbose mode")

    args = parser.parse_args()

    # Dynamic loading of the requested site adapter
    IbNetPyGraph = getattr(importlib.import_module(args.adapter), args.adapter)

    full_path = args.dst

    # Start attr is used as the initial seed for nodes placements, use the same
    # value to keep a similar graph between each run.
    graph = pydot.Dot(
        graph_type='graph',
        ratio="expand",
        size="10,10!",
        mode="major",
        outputorder="edgesfirst",
        overlap='vpsc',
        quadtree=True,
        spline=False,
        epsilon="0.00001",
        start="1",
        maxiter=2000,
        normalize=True,
        sep="+4,4",
        id='viewport')
    if args.verbose:
        print "Parse ibNetDiscover file"
    ibNetDict = readIbNet(args.ibnetdiscover)
    if args.verbose:
        print "End to parse ibNetDiscover file"

    if args.verbose:
        print "Generate Nodes and Path"
    IbNetPyGraph(ibNetDict['id'], graph).createGraph()
    if args.verbose:
        print "End to generate Nodes and Path"

    if args.verbose:
        start = time.time()
    if args.verbose:
        print "Generate grap"
    graph.write(full_path, prog="neato", format='svg')
    if args.verbose:
        print "End to generate graph: ", time.time() - start, "seconds"

if __name__ == "__main__":
    main()
