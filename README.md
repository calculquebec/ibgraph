IB Graph
========

Dependencies
------------
* pydot

Generating files
----------------

[Colosse](https://www.calculquebec.ca/en/resources/compute-servers/colosse)'s Infiniband network is used as an example:

    ./graphGenerator.py -s Colosse res/colosse/ibnetdiscover.txt /tmp/graph.svg
    ./JSONMaker.py -c -e ./res/colosse/iberrs > /tmp/iberrs.json
    ./JSONMaker.py -c -i ./res/colosse/ibnetdiscover.txt > /tmp/graphInfo.json
    ./JSONMaker.py -c -q ./res/colosse/lfts.20130625 ./res/colosse/ibnetdiscover.txt > /tmp/lftsQNEM.json

Interface
---------
![Symbol errors](https://bitbucket.org/calculquebec/ibgraph/raw/master/examples/colosse/interface_symbolerrors.png)
![Symbol errors](https://bitbucket.org/calculquebec/ibgraph/raw/master/examples/colosse/interface_routes_DST.png)
![Symbol errors](https://bitbucket.org/calculquebec/ibgraph/raw/master/examples/colosse/interface_routes.png)
