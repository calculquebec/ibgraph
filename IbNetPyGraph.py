#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import pydot


class IbNetPyGraph:
    """This class should be used as a base class(heritage) to create another
    class specific to a Infiniband Fabric using the correct physical and/or
    logical emplacement in the graph.
    """
    def __init__(self, ibNetDict, graph):
        self.ibNetDict = ibNetDict
        self.graph = graph
        self.dictSubGraph = dict()
        self.i = 0
        self.groupedNodes = []

        self.HSV_tuples = self.generateHSV()

    def generateHSV(self, N=10):
        """N is the number of colors to use in the entire graph. N value should
        be higher than the number of elements (minus normal nodes).
        """
        return [("%lf" % (x * 1.0 / N), "%lf" % (0.5), "%lf" % (1.0)) for x in range(N)]

    def removeDoubleEdge(self):
        """Remove bidirectionnals links found in IbNetDiscover."""
        for key, value in self.ibNetDict.items():
            for numPort, infoPort in value['port'].items():
                success = self.ibNetDict[infoPort['ib']]['port']\
                    .pop(infoPort['port'], False)
                if not success:
                    raise KeyError("Enable to delete %s" % infoPort['port'])

    def nodeAttr(self, key, value, keygrp):
        """Return a dict of attributes for a node."""
        tooltipValue = "Name: " + value['name'] + "&#13;ID: " + key + "&#13;LID: " + value['lid']
        return {
            'id': key,
            'pin': False,
            'tooltip': tooltipValue,
            'label': value['name'],
            'fillcolor': self.dictSubGraph[keygrp]['node']['color'],
            'pos': self.dictSubGraph[keygrp]['node']['pos'],
            'style': "filled"
        }

    def createNodes(self):
        """Create all nodes found in ibNetDict."""
        for key, value in self.ibNetDict.items():
            for keygrp in self.dictSubGraph.keys():
                if keygrp in value['name']:
                    attr = self.nodeAttr(key, value, keygrp)
                    if not self.dictSubGraph[keygrp]['subgraph'].get_node(attr['id']):
                        self.dictSubGraph[keygrp]['subgraph'].add_node(
                            pydot.Node(
                                name=attr['id'],
                                label=attr['label'],
                                style=attr['style'],
                                id=attr['id'],
                                fillcolor=attr['fillcolor'],
                                tooltip=attr['tooltip'],
                                pos=attr['pos'],
                                pin=attr['pin'],
                            )
                        )
                    break
            else:
                self.graph.add_node(pydot.Node(
                    name=key,
                    id=key,
                    label=value['name'],
                    style='filled',
                    fillcolor='red',
                    tooltip="Name: " + value['name'] + "&#13;ID: " + key
                    + "&#13;LID: " + value['lid'])
                )

    def createInvNodes(self):
        """Create invisibles nodes to regroup childs."""
        for keygrp in self.dictSubGraph.keys():
            self.graph.add_node(pydot.Node(
                keygrp + 'inv',
                style="invis"))

    def createEdges(self):
        """Create edges between nodes based on items in ibNetDict."""
        for key, value in self.ibNetDict.items():
            for numPort, infoPort in value['port'].items():
                find = False

                for keygrp in self.dictSubGraph.keys():
                    #link QNEM together
                    if keygrp in value['name'] and keygrp in self.ibNetDict[infoPort['ib']]['name']:
                        self.graph.add_edge(pydot.Edge(
                            key,
                            infoPort['ib'],
                            id=key + ':%02d' % int(numPort) + "--" + infoPort['ib'] + ':%02d' % int(infoPort['port']),
                            color='black',
                            weight=10,
                            len="2.0",
                            tooltip=value['name'] + ':%02d' % int(numPort)
                            + " -- " + self.ibNetDict[infoPort['ib']]['name']
                            + ':%02d' % int(infoPort['port'])))
                        find = True
                        break

                    elif keygrp in value['name'] or keygrp in self.ibNetDict[infoPort['ib']]['name']:
                        if keygrp in value['name']:
                            for keygrp2 in self.dictSubGraph.keys():
                                if keygrp2 in self.ibNetDict[infoPort['ib']]['name']:
                                    # Remove 'cluster' from the group's name
                                    nameNode = self.dictSubGraph[keygrp2]['subgraph'].get_name()[8:]
                                    break
                            else:
                                nameNode = infoPort['ib']

                        else:
                            for keygrp2 in self.dictSubGraph.keys():
                                if keygrp2 in value['name']:
                                    nameNode = self.dictSubGraph[keygrp2]['subgraph'].get_name()[8:]
                                    break
                            else:
                                nameNode = key
                        if nameNode == key or nameNode == infoPort['ib']:
                            # Get the initial position
                            x, y = self.dictSubGraph[keygrp]['node']['pos'].split(',', 1)

                            for node in self.graph.get_node('"%s"' % nameNode):
                                node.set_pos("%f,%f" % (float(x) * 1, float(y) * 1))

                            #Create invisble edge for grouping node together
                            if any(name in keygrp for name in self.groupedNodes):
                                for node in self.dictSubGraph[keygrp]['subgraph'].get_node_list():
                                    self.graph.add_edge(pydot.Edge(
                                        keygrp + 'inv',
                                        nameNode,
                                        style='invis',
                                        len="5.5",
                                        weight="0.51"))

                        self.graph.add_edge(pydot.Edge(
                            infoPort['ib'],
                            key,
                            id=key + ':%02d' % int(numPort) + "--" + infoPort['ib'] + ':%02d' % int(infoPort['port']),
                            color='black',
                            len="15.0",
                            weigth="0.5",
                            tooltip=value['name'] + ':%02d' % int(numPort) + " -- " + self.ibNetDict[infoPort['ib']]['name'] + ':%02d' % int(infoPort['port'])))

                        find = True
                        break

                if not find:
                    self.graph.add_edge(pydot.Edge(
                        key,
                        infoPort['ib'],
                        id=key + ':%02d' % int(numPort) + "--" + infoPort['ib'] + ':%02d' % int(infoPort['port']),
                        len="8.0",
                        edgeTooltip=value['name'] + ':%02d' % int(numPort) + " -- " + self.ibNetDict[infoPort['ib']]['name'] + ':%02d' % int(infoPort['port']))
                    )

    def createClusters(self):
        """This function should create each cluster of nodes according to a
        predefined topology.

        Example to create one cluster called 'aCluster0':
        self.dictSubGraph.setdefault(
            "aCluster0",
            {
                'subgraph': pydot.Cluster(
                    "aCluster0",
                    fillcolor=",".join(self.HSV_tuples[self.i]),
                    style='filled',
                    tooltip="aCluster",
                    id="aCluster0",
                'node': {
                    'color': ",".join(self.HSV_tuples[self.i]),
                    'pos': "0,0",
                    'len': 1
                }
            })
        """
        raise NotImplementedError

    def createGraph(self):
        self.removeDoubleEdge()
        self.createClusters()
        self.createNodes()
        self.createInvNodes()
         # Add SubGraph (Cluster) to Graph
        for value in self.dictSubGraph.values():
            self.graph.add_subgraph(value['subgraph'])
        self.createEdges()
