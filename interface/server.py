#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import argparse
import BaseHTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", action="store", default=8001, type=int,
                    help="web server port")
args = parser.parse_args()

HandlerClass = SimpleHTTPRequestHandler
ServerClass = BaseHTTPServer.HTTPServer
Protocol = "HTTP/1.0"

server_address = ('127.0.0.1', args.port)

HandlerClass.protocol_version = Protocol
httpd = ServerClass(server_address, HandlerClass)

sa = httpd.socket.getsockname()
print "Serving HTTP on", sa[0], "port", sa[1], "..."
httpd.serve_forever()
