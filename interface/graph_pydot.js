$(document).ready(function(){
  $("#svgload").svg({
      loadURL: 'graph.svg',
      onLoad: loadDone,
      settings: {}
    }
  );

  function loadDone() {
    var svg = $('#svgload').svg('get');

    //Set the same viewbox of div and svg
    var width = $('#svgload').width();
    var height = $('#svgload').height();

    //Need for zoom and pan
    $(svg.root()).attr('viewBox', '0 0 ' + width + ' ' + height);
    //Zoom and pad
    $(svg.root()).svgPan('viewport');

    //Put listener on node and cluster
    $('.node', svg.root()).bind('mouseenter', svgNodeOver)
                          .bind('mouseleave', svgNodeOut);
    //Remove graph tooltip
    $('.graph', svg.root()).find('title').text('');

    $.getJSON('json/graphInfo.json',function(data) {
      var id;
      for(id in data.id){
        if(mapNameIdLid[data.id[id].name]){
          mapNameIdLid[data.id[id].name]['id'].push(id);
          mapNameIdLid[data.id[id].name]['lid'].push(data.id[id].lid);
        }
        else{
          mapNameIdLid[data.id[id].name] = {
            'id': [id],
            'lid': [data.id[id].lid]
          };
        }
        listname.push(data.id[id].name);
        mapLidId[data.id[id].lid] = id;

        $($('.cluster'), svg.root()).each(function(){
          if(data.id[id].name.indexOf($(this).attr('id')) != -1 ){
            if(!mapClusterId[$(this).attr('id')]){
              mapClusterId[$(this).attr('id')] = [];
              // Patch: cluster M2-X and node M2-X have the same name
              if($(this).attr('id').indexOf("M2") == -1){
                listname.push($(this).attr('id'));
              }
            }
            mapClusterId[$(this).attr('id')].push(id);
          }
        });
      }
    });
    $("#clearbtn").trigger('click');
  }

  var mapLidId      = {};
  var mapClusterId  = {};
  var mapNameIdLid  = {};
  var listname      = [];
  var blinkID       = [];
  var interval      = false;

  /**
   *  jQuery textfield
   */
  $('#search, #dstfield').typeahead({
    source: listname,
    items: 999
  });

  $("#searchbtn").click(function(){
    var svg = $('#svgload').svg('get');
    if (mapNameIdLid[$("#search").val()]){
      var id = mapNameIdLid[$("#search").val()]['id'];
      var i;
      for (i in id){
        $('#' + id[i], svg.root() ).addClass('searched');
        $('[id*="' + id[i] + '"]', svg.root() ).addClass('searched');
      }
    }
    else if(mapClusterId[$("#search").val()]){
      $('#' + $("#search").val(), svg.root() ).addClass('searched');
      for(i in mapClusterId[$("#search").val()]){
        $('#' + mapClusterId[$("#search").val()][i], svg.root()).addClass('searched');
        $('[id*="' + mapClusterId[$("#search").val()][i] + '"]', svg.root() ).addClass('searched');
      }
    }
  });

  $("#rcverrbtn").click({param1: 'rcverrors'}, showErrors);
  $("#smberrbtn").click({param1: 'symbolerrors'}, showErrors);
  $("#lnkdwnbtn").click({param1: 'linkdowned'}, showErrors);
  $("#lnkrcvbtn").click({param1: 'linkrecovers'}, showErrors);
  $("#vl15drbtn").click({param1: 'vl15dropped'}, showErrors);

  $("#rteqnembtn").click(function(){
    var btn = $(this);
    btn.button('loading');
    $("#clearbtn").trigger('click');

    var svg = $('#svgload').svg('get');
    var rainbow = new Rainbow();
    rainbow.setSpectrum('green', 'yellow', 'red');

    $.getJSON('json/lftsQNEM.json', function(lfts){
      rainbow.setNumberRange(lfts.min, lfts.max);
      var lid, port;
      for(lid in lfts.lid){
        for(port in lfts.lid[lid]){
          $('[id*="' + mapLidId[lid] + ':' + pad(port) + '"]', svg.root())
            .attr('class', "edge")
            .find('path')
            .attr('stroke', '#' + rainbow.colourAt(lfts.lid[lid][port]))
            .attr('stroke-width', '3');
        }
      }
      makeGauge(lfts.min,lfts.max, 'routes');
      btn.button('reset');
    });
  });

  $("#clearbtn").click(function(){
    var svg = $('#svgload').svg('get');
    $('.node', svg.root()).attr('class', "node std");
    $('.edge', svg.root()).attr('class', "edge std");
    $('.cluster', svg.root()).attr('class', "cluster");
    if($('#gradient', svg.root()).exists()){
      svg.remove($('#gradient', svg.root()));
    }
    clearInterval(interval);
    blinkID = [];
  });

  // Only if Name = 1 id and 1 lid
  $("#shdstbtn").click(function(){
    if(mapNameIdLid[$("#dstfield").val()]){
      var btn = $(this);
      btn.button('loading');
      $("#clearbtn").trigger('click');

      $.getJSON('json/lfts.json', function(lfts){
        if(mapNameIdLid[$("#dstfield").val()]['lid'].length > 1 ||
           mapNameIdLid[$("#dstfield").val()]['id'].length > 1){
          console.log('More than 1 lid or 1 id, try with other name');
        }
        else{
          var svg = $('#svgload').svg('get');
          var dst = mapNameIdLid[$("#dstfield").val()]['lid'][0];
          $('#' + mapNameIdLid[$("#dstfield").val()]['id'][0]).addClass('routed');
          var lid;
          for (lid in lfts.lid){
            //Pour s'assurer qu'il existe
            if(lfts.lid[lid][dst]){
              $('[id*="' + mapLidId[lid] + ':' + pad(lfts.lid[lid][dst]) + '"]', svg.root())
              .addClass('routed');
            }
            else if(lid != dst){
              console.log("lid '" + lid + "' don't have lid '" + dst + "' for destination");
            }
          }
        }
        btn.button('reset');
      });
    }
    else{
      console.log("Destination doesn't exist");
    }
  });


  function showErrors(event){
    var btn = $(this);
    btn.button('loading');

    $("#clearbtn").trigger('click');
    //Pas optimale car il retelecharge toujours le fichier,
    //mais permet de definir plusieurs fichier different de iberr dans le temps
    //TODO: Implanter plusieurs fichier de temps pour alleger les requetes
    $.getJSON('json/iberrs.json', function(iberrs){
      var maxval = 0;
      var minval = 1000;
      var val = [];
      var linkErrDict = {};
      var nbHours = $('#errperiod').val();
      var svg = $('#svgload').svg('get');
      var hour, guid, port;
      for(hour in iberrs){
        if((nbHours >= hour) || (nbHours == -1)){
          for(guid in iberrs[hour]){
            for(port in iberrs[hour][guid]){
              if(iberrs[hour][guid][port][event.data.param1]){
                $('[id*="' + guid + ':' + pad(port) + '"]', svg.root()).each(function(){
                  if(linkErrDict[$(this).attr('id')]){
                    linkErrDict[$(this).attr('id')] += Number(iberrs[hour][guid][port][event.data.param1]);
                  }
                  else {
                    linkErrDict[$(this).attr('id')] = Number(iberrs[hour][guid][port][event.data.param1]);
                  }
                });
              }
            }
          }
        }
        else{
          break;
        }
      }

      var key;
      for(key in linkErrDict){
        val.push(linkErrDict[key]);
      }

      var rainbow = new Rainbow()
      rainbow.setSpectrum('green', 'yellow', 'red');

      maxval = quantile(val, 19, 20);
      minval = quantile(val, 1 , 20);
      if(maxval == minval) {
        minval -= 1;
      }
      if(isNaN(maxval) || isNaN(minval)) {
        makeGauge(0,0, event.data.param1);
      }
      else {
        rainbow.setNumberRange(minval, maxval);
        makeGauge(minval,maxval, event.data.param1);
      }

      var link;
      for(link in linkErrDict) {
        var tooltip = $($('[id="' + link + '"]'), svg.root())
          .find('a')
          .attr('xlink:title');

        //Find and delete same event on tooltip
        var indStart = tooltip.indexOf(event.data.param1)
        if(indStart != -1){
          var indEnd = tooltip.indexOf('\n', indStart);
          if (indEnd == -1){
            indEnd = Number.MAX_VALUE;
          }
          var strSearched = tooltip.substring(indStart, indEnd)
          tooltip = tooltip.split('\n');
          tooltip.splice(tooltip.indexOf(strSearched), 1);
          tooltip = tooltip.join('\n');
        }

        blinkID.push(link);

        $('[id="' + link + '"]', svg.root())
          .attr('class', "edge error")
          .find('a')
          .attr('xlink:title', tooltip + '\n' + event.data.param1 + ':' + linkErrDict[link])
          .find('path')
          .attr('stroke','#' + rainbow.colourAt(linkErrDict[link]))
          .attr('stroke-width', '20')
          .attr('opacity', '0.5');
      }
      btn.button('reset');
    });
  }

  $("#blinkbtn").click(function(){
    interval = setInterval(blinkLink , 500);
  });
  $("#stopbtn").click(function(){
    //remettre l'état initial
    var svg = $('#svgload').svg('get');
    var id;
    for(id in blinkID){
      var link = $('[id="' + blinkID[id] + '"]', svg.root());
      if(link.hasClass('std')) {
        link.removeClass('std');
      }
    }
    clearInterval(interval);
  });

  function blinkLink(){
    var svg = $('#svgload').svg('get');
    var nextState = 'visible';
    var id;
    for(id in blinkID) {
      var link = $('[id="' + blinkID[id] + '"]', svg.root());
      if(link.hasClass('std')) {
        link.removeClass('std');
      }
      else {
        link.addClass('std');
      }
    }
  }

  /**
  *   Draw gauge
  **/
  function makeGauge(minval, maxval, errorType){
    var svg = $('#svgload').svg('get');
    var x = 10;
    var y = 25;
    var dx = 200;
    var dy = 25;
    var g = svg.group(
      'gradient',
      {
        stroke: 'black',
        strokeWidth: 2,
        textAnchor:'middle'
      });
    var defs = svg.defs(g);

    svg.linearGradient(
      defs,
      'gaugeGradient',
      [[0, 'green'], [0.5, 'yellow'], [1, 'red']],
      0,
      0,
      dx,
      0,
      {
        gradientUnits: 'userSpaceOnUse'
      });
    svg.rect(g, x, y, dx, dy,{ fill: 'url(#gaugeGradient)' });
    svg.text(g, 'Number of '+ errorType, {
      x: x + dx / 2,
      y: y + dy / 2 - 20,
      strokeWidth:'0.1px'
    });
    svg.text(g, minval.toString(), {
      x: x,
      y: y + dy + 15,
      strokeWidth: '0.1px'
    });
    svg.text(g, ((minval + maxval) / 2).toString(),{
      x: x + dx / 2,
      y: y + dy + 15,
      strokeWidth: '0.1px'
    });
    svg.text(g, maxval.toString(), {
      x: x + dx,
      y: y + dy + 15,
      strokeWidth:'0.1px'
    });
  }


  /***********Listener***********/

  /**
  *   When mouse is over node
  **/
  function svgNodeOver() {
    var svg = $('#svgload').svg('get');
    $(this).addClass('mouseover')
    $('[id*="' + $(this).attr('id') + '"][class*="edge"]', svg.root()).addClass('mouseover')
  }

  /**
  *   When mouse is out node
  **/
  function svgNodeOut() {
    var svg = $('#svgload').svg('get');
    $(this, svg.root()).removeClass('mouseover')
    $('[id*="' + $(this).attr('id') + '"][class*="edge"]', svg.root()).removeClass("mouseover");
  }

  /***********Utility***********/

  jQuery.fn.exists = function(){
    if(this.length > 0){
      return this;
    }
    else {
      return false;
    }
  }

  /**
  /* Parse transform attribut
  /**/
  function parseTransform(a)
  {
    var b = {};
    for (var i in a = a.match(/(\w+\((\-?\d+\.?\d* ?,?)+\))+/g)){
      var c = String(a[i]).match(/[\w\.\-]+/g);
      b[c.shift()] = c;
    }
    return b;
  }

  /**
  /*  Mathematic function
  /**/
  function pad(num) {
    var s = "00" + num;
    return s.substr(s.length-2);
  }

  /**
  /*  Mathematic function - Find quantile
  /**/
  function quantile(arr, k, q) {
    var sorted, count, index;

    if (k === 0) return Math.min.apply(null, arr);

    if (k === q) return Math.max.apply(null, arr);

    sorted = arr.slice(0);
    sorted.sort(function (a, b) {
      return a - b;
    });
    count = sorted.length;
    index = count * k / q;

    if (index % 1 === 0) return 0.5 * sorted[index - 1] + 0.5 * sorted[index];

    return sorted[Math.floor(index)];
  }
});
