#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import json
import argparse
import utilParser as m


def main():
    parser = argparse.ArgumentParser(
        description='JSON Maker for webPydot and webD3JS')

    group = parser.add_mutually_exclusive_group()

    parser.add_argument(
        "file",
        help="File to parse")

    group.add_argument(
        "-e", "--err",
        action="store_true",
        dest="iberrs",
        default=False,
        help="Enable iberrs parser")

    group.add_argument(
        "-i", "--ibdisc",
        action="store_true",
        dest="ibnet",
        default=False,
        help="Enable IbNetDiscover parser")

    group.add_argument(
        "-l", "--lfts",
        action="store_true",
        dest="lfts",
        default=False,
        help="Enable lfts parser")

    group.add_argument(
        "-q", "--lftsq",
        action="store",
        dest="lftsqnem",
        default="",
        help="Enable lfts parser. Given lfts file in arg and ibnetdiscover.txt in positional")

    group.add_argument(
        "-d", "--d3js",
        action="store_true",
        dest="d3js",
        default=False,
        help="Enable D3JS parser")

    parser.add_argument(
        "-c", "--comp",
        action="store_true",
        dest="compress",
        default=False,
        help="Enable compression of JSON")

    args = parser.parse_args()

    valIndent = 2
    if (args.compress):
        valIndent = None

    if args.iberrs:
        dataDict = m.readIbErrs(args.file, -1)

    elif args.d3js:
        ibNetDict = m.readIbNet(args.file)
        print m.makeJSOND3JS(ibNetDict['id'])
        return

    elif args.ibnet:
        dataDict = m.readIbNet(args.file)

    elif args.lfts:
        dataDict = m.readlfts(args.file)

    elif args.lftsqnem:
        lftsDict = m.readlfts(args.lftsqnem)
        ibNetDict = m.readIbNet(args.file)
        mapQNEMLidId = {}

        lftsQNEMDict = m.lftsKeepQNEM(ibNetDict, lftsDict, mapQNEMLidId)
        lidTimesPort = m.evalNbTimesPort(lftsQNEMDict)

        dataDict = m.fusionPortSameQNEM(ibNetDict, lidTimesPort, mapQNEMLidId)
    else:
        dataDict = {}

    print json.dumps(
        dataDict,
        sort_keys=True,
        indent=valIndent,
        separators=(',', ': '))


if __name__ == "__main__":
    main()
