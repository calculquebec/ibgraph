#!/usr/bin/env python
# -*- coding: UTF-8 -*-


def readIbNet(filename):
    try:
        with open(filename, 'r') as ibNetFile:
            return parserToDict(ibNetFile)

    except IOError:
        print "%s: Bad file name !" % filename
        exit(1)


def parserToDict(ibNetFile):
    switchIbNetDict = dict()
    caIbNetDict = dict()

    for block in ibNetFile.read().split("\n\n"):
        dictTemp = dict().fromkeys(["port"], dict())
        for line in block.split("\n"):

            if "vendid" in line[0:6]:
                dictTemp.setdefault('vendid', line[7:])
            elif "devid" in line[0:5]:
                dictTemp.setdefault('devid', line[6:])
            elif "sysimgguid" in line[0:10]:
                dictTemp.setdefault('sysimgguid', line[11:])
            elif "switchguid" in line[0:10]:
                dictTemp.setdefault('switchguid', line[11:line.find('(')])
            elif "caguid" in line[0:6]:
                dictTemp.setdefault('caguid', line[7:])

            elif "Switch" in line[0:6]:
                dictTemp.setdefault('nbport', line[7:line.find(' ')])
                if len(line.split('"')) > 0:
                    switchIbNetDict.setdefault(line.split('"')[1], dictTemp)
                if line[line.find('#'):].split('"') > 0:
                    dictTemp.setdefault('name', line[line.find('#'):].split('"')[1])
                    dictTemp.setdefault('lid', line[line.find('lid'):].split()[1])

            elif "Ca" in line[0:2]:
                dictTemp.setdefault('nbport', line[3:line.find(' ')])
                if len(line.split('"')) > 0:
                    caIbNetDict.setdefault(line.split('"')[1], dictTemp)
                if line[line.find('#'):].split('"') > 0:
                    dictTemp.setdefault('name', line[line.find('#'):].split('"')[1])

            elif '[' in line[0:1]:
                dictPortTemp = dictTemp["port"].setdefault(line[1:line.find(']')], dict())
                if line.split('"') > 0:
                    dictPortTemp.setdefault('ib', line.split('"')[1])
                dictPortTemp.setdefault('port', line[line.rfind('[') + 1:line.rfind(']')])
                if line.count('lid') == 1:
                    dictPortTemp.setdefault('lid', line[line.find('lid'):].split()[1])
                elif line.count('lid') == 2:
                    dictTemp.setdefault('lid', line[line.find('lid'):].split()[1])
                    dictPortTemp.setdefault('lid', line[line.rfind('lid'):].split()[1])

    ibNetDict = {
        'id': {}
    }
    ibNetDict['id'].update(switchIbNetDict)
    ibNetDict['id'].update(caIbNetDict)
    return ibNetDict


def pretty(d, indent=0):
    for key, value in d.iteritems():
        print '\t' * indent + str(key)
        if isinstance(value, dict):
            pretty(value, indent + 1)
        else:
            print '\t' * (indent + 1) + str(value)


def makeJSOND3JS(ibNetDict):
    nodeList = []

    for key, value in ibNetDict.items():
        for numPort, infoPort in value['port'].items():
            success = True
            success = ibNetDict[infoPort['ib']]['port'].pop(infoPort['port'], False)
            if not success:
                str = "impossible de supprimer %s" % infoPort['port']
                print str
                exit(1)

    dictSubGraph = {}
    nbFC = 2  # Start to 1
    nbLCbyFC = 7 + 1  # Start to 0
    nbRACK = 10  # Start to 1
    nbQNEMbyRACK = 4  # Start to 1
    nbM2 = 2  # Start to 1

    group = 1
    color = 1
    for i in range(nbFC):
        dictSubGraph.setdefault(
            "M9-%d_FC" % (i + 1),
            {
                'group': group,
                'color': color
            })
        group += 1
        color += 1
        for j in range(nbLCbyFC):
            dictSubGraph.setdefault(
                "M9-%d_LC%d" % (i + 1, j),
                {
                    'group': group,
                    'color': color
                })
            group += 1
            color += 1

    for i in range(nbRACK):
        #dictSubGraph.setdefault("RACK-1%02d" %(i+1), {'group': group,'color':color} )
        #group += 1
        for j in range(nbQNEMbyRACK):
            dictSubGraph.setdefault(
                "RACK-1%02d_QNEM-%d" % (i + 1, j + 1),
                {
                    'group': group,
                    'color': color
                })
            group += 1
        color += 1

    for i in range(nbM2):
            dictSubGraph.setdefault(
                "M2-%d" % (i + 1),
                {
                    'group': group,
                    'color': color
                })
            group += 1
            color += 1

    dictSubGraph.setdefault(
        "Xyratex_IS5035U1",
        {
            'group': group,
            'color': color
        })
    color += 1
    group += 1

    valgroup = 0
    for key, value in ibNetDict.items():
        for grpkey in dictSubGraph.keys():
            if grpkey in value['name']:
                valgroup = dictSubGraph[grpkey]['group']
                valcolor = dictSubGraph[grpkey]['color']
                break
        else:
            valgroup = group
            valcolor = color
            group += 1

        s = "    {\"id\":\"%s\",\"name\":\"%s\",\"nbport\":%d,\"group\":%d,\"color\":%d}" \
            % (key, value['name'], int(value['nbport']), valgroup, valcolor)
        nodeList.append(s)
    nodestr = ",\n".join(nodeList)

    linkList = []
    for key, value in ibNetDict.items():
        for numPort, infoPort in value['port'].items():
            s = "    {\"source\":\"%s\",\"sport\":\"%s\",\"target\":\"%s\",\"tport\":\"%s\"}" \
                % (key, numPort, infoPort['ib'], infoPort['port'])
            linkList.append(s)

    linkstr = ",\n".join(linkList)
    return "{\n  \"nodes\":[\n%s\n  ],\n  \"links\":[%s\n  ]\n}" % (nodestr, linkstr)


def ibErrsParse(ibErrsFile, nbHours):
    current = 0
    hour = 0
    ibErrsDict = {hour: {}}

    for line in ibErrsFile:
        if nbHours >= current or nbHours == -1:
            line = line.strip()
            if "***** ERRORS collected" in line[0:22]:
                hour += 1
                ibErrsDict.setdefault(hour, {})
                current += 1
            elif "Errors" in line[0:6]:
                pass
            elif "GUID" in line[0:4]:
                info, errors = line.split(':', 1)

                info = info.split()
                guid = '%0.16x' % int(info[1], 16)
                port = info[3]
                errList = errors.split('[')
                for err in errList:
                    elem = err.split()
                    if "SymbolErrorCounter" in elem:
                        if int(elem[2].strip(']')) != 65535:
                            ibErrsDict[hour].setdefault(guid, {})\
                                .setdefault(port, {}).setdefault("symbolerrors", 0)
                            ibErrsDict[hour][guid][port]["symbolerrors"] += int(elem[2].strip(']'))
                    elif "PortRcvErrors" in elem:
                        if int(elem[2].strip(']')) != 65535:
                            ibErrsDict[hour].setdefault(guid, {})\
                                .setdefault(port, {}).setdefault("rcverrors", 0)
                            ibErrsDict[hour][guid][port]["rcverrors"] += int(elem[2].strip(']'))
                    elif "LinkDownedCounter" in elem:
                        if int(elem[2].strip(']')) != 65535:
                            ibErrsDict[hour].setdefault(guid, {})\
                                .setdefault(port, {}).setdefault("linkdowned", 0)
                            ibErrsDict[hour][guid][port]["linkdowned"] += int(elem[2].strip(']'))
                    elif "LinkErrorRecoveryCounter" in elem:
                        if int(elem[2].strip(']')) != 65535:
                            ibErrsDict[hour].setdefault(guid, {})\
                                .setdefault(port, {}).setdefault("linkrecovers", 0)
                            ibErrsDict[hour][guid][port]["linkrecovers"] += int(elem[2].strip(']'))
                    elif "VL15Dropped" in elem:
                        if int(elem[2].strip(']')) != 65535:
                            ibErrsDict[hour].setdefault(guid, {})\
                                .setdefault(port, {}).setdefault("vl15dropped", 0)
                            ibErrsDict[hour][guid][port]["vl15dropped"] += int(elem[2].strip(']'))

            elif "Link" in line[0:4]:
                pass
            else:
                pass

        else:
            break
    return ibErrsDict


def readIbErrs(filename, nbHours):
    try:
        with open(filename, 'r') as ibErrsFile:
            return ibErrsParse(ibErrsFile, nbHours)

    except IOError:
        print "%s: Bad file name !" % filename
        exit(1)


def lftsParse(lftsFile):
    lftsDict = {'lid': {}}
    lid = 'NONE'

    for line in lftsFile:
        if "Unicast" in line[0:7]:
            lid = line.split('Lid', 1)[1].split()[0]
            lftsDict['lid'].setdefault(lid, {})
        elif "0x" in line[0:2]:
            lineList = line.split()
            lftsDict['lid'][lid].setdefault(int(lineList[0], 16), int(lineList[1]))
        pass
    return lftsDict


def readlfts(filename):
    try:
        with open(filename, 'r') as lftsFile:
            return lftsParse(lftsFile)
    except IOError:
        print "%s: Bad file name !" % filename
        exit(1)


def lftsKeepQNEM(ibNetDict, lftsDict, mapQNEMLidId):
    lftsQNEMDict = {'lid': {}}

    for key, value in ibNetDict['id'].items():
        if "QNEM" in value['name']:
            if value['lid'] in lftsDict['lid']:
                mapQNEMLidId.setdefault(value['lid'], key)
                lftsQNEMDict['lid'].setdefault(value['lid'], lftsDict['lid'][value['lid']])

    return lftsQNEMDict


def evalNbTimesPort(lftsDict):
    lidTimesPort = {'lid': {}, 'max': 0, 'min': 1}

    for key, value in lftsDict['lid'].items():
        lidTimesPort['lid'].setdefault(key, {})

        for port in value.values():
            lidTimesPort['lid'][key].setdefault(port, 0)
            lidTimesPort['lid'][key][port] += 1
            if lidTimesPort['lid'][key][port] > lidTimesPort['max']:
                lidTimesPort['max'] = lidTimesPort['lid'][key][port]

    if lidTimesPort['max'] == lidTimesPort['min']:
        lidTimesPort['min'] -= 1
    return lidTimesPort


def fusionPortSameQNEM(ibNetDict, lidTimesPort, mapQNEMLidId):
    for lid, timesPort in lidTimesPort['lid'].items():
        for port, infoPort in ibNetDict['id'][mapQNEMLidId[lid]]['port'].items():
            if "QNEM" in ibNetDict['id'][infoPort['ib']]['name']:
                if infoPort['lid'] in lidTimesPort['lid'] and int(port) in timesPort:
                    if int(infoPort['port']) in lidTimesPort['lid'][infoPort['lid']]:
                        timesPort[int(port)] += lidTimesPort['lid'][infoPort['lid']][int(infoPort['port'])]
                        if timesPort[int(port)] > lidTimesPort['max']:
                            lidTimesPort['max'] = timesPort[int(port)]
                        lidTimesPort['lid'][infoPort['lid']].pop(int(infoPort['port']))

    return lidTimesPort
